# Project 5: Brevet time calculator with Ajax and MongoDB
Author: Brittany Waiters; bwaiters@uoregon.edu

## Controle Time calculation
Based on the information on https://rusa.org/pages/acp-brevet-control-times-calculator

The greatest distance is 1000 while the smallest is between 0 and 200. If the control distance is anything greater than 200 we calculate by checking how big the difference is working backwards from the greatest distance which is 1000. For the opening control times you divide by the maximum speed in km/hr. For the close times it is the same procedure but rather than dividing by the maximum speed we divide by the minimum speed in km/hr given in the table. We do this all the way down the table being sure to add to the running total which is returned at the end. The total returned is the control open or close time.

In addition, if the control distance is a predetermined distance for a brevet then there is a specific closing time that overrides the total we calculate.

## Display
Controle times entered, calculated and then stored when submit is pressed. When display is pushed then it will display brevet times

## Test Cases
-Submit is pushed with no controle times entered then display error message
-Display is pushed with no controle times entered then would say "nothing submitted"
